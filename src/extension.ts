import { commands, ExtensionContext, languages, Position, Range, TextDocument, window } from "vscode";
import { CypressLensProvider, getTextWithoutComments } from "./cypress-lens-provider";
// this method is called when your extension is activated
// your extension is activated the very first time the command is executed

const emptyRegex = new RegExp(/\b(describe|context|it) *\(/g);
const fullRegex = new RegExp(/\b(describe|context|it) *(\.skip|\.only)\(/g);


const createEditCommand = (context: ExtensionContext, command: string, replace: string) => {
    context.subscriptions.push(commands.registerCommand(command,
        (document: TextDocument, range: Range) => {
            window.visibleTextEditors.find(x => x.document === document)?.edit((builder) => {
                if (range.start.isEqual(range.end)) {
                    if (document.lineAt(range.start.line).text.match(emptyRegex)) {
                        builder.insert(range.start, replace);
                    }
                    return;
                }
                if (document.lineAt(range.start.line).text.match(fullRegex)) {
                    builder.replace(range, replace);
                }
            });
        }));
};

const foldChildren = (document: TextDocument, range: Range, recursive: boolean): Position[] => {
    const positions: Position[] = [];
    const regex = new RegExp(/\b(describe|context|it) *(\.skip|\.only| *)\(/);

    let depth = 0;
    let blockDepth: number[] = [];

    const text = getTextWithoutComments(document.getText(new Range(range.end, document.lineAt(document.lineCount - 1).range.end)));
    const lines = text.split('\n');
    let offset = document.getText(new Range(document.lineAt(0).range.start, range.end)).length;
    for (let l = 0; l < lines.length; l++) {
        const line = lines[l];
        const length = line.length;
        for (let i = 0; i < length; i++) {
            if (line.charAt(i) === '{') {
                depth++;
            } else if (line.charAt(i) === '}') {
                depth--;
                if (depth === 0) {
                    break;
                }
            }
        }
        if (depth === 0) {
            break;
        }

        if (regex.test(line)) {
            while (blockDepth.length > 0 && blockDepth[blockDepth.length - 1] >= depth) {
                blockDepth.pop();
            }
            if ((recursive) || blockDepth.length === 0) {
                positions.push(document.positionAt(offset));
            }
            blockDepth.push(depth);
        }
        offset += length + 1;
    }


    return positions;
};

export function activate(context: ExtensionContext) {
    const cypressLensProvider = new CypressLensProvider();
    languages.registerCodeLensProvider("typescript", cypressLensProvider);
    languages.registerCodeLensProvider("javascript", cypressLensProvider);

    context.subscriptions.push(commands.registerCommand('cypresshelper.foldChildren',
        (document: TextDocument, range: Range, recursive: boolean) => {
            const positions = foldChildren(document, range, recursive);

            if (positions.length === 0) { return; }
            // const diff = range.start.line - (window.activeTextEditor?.selection.active.line ?? range.start.line);
            // // window.visibleTextEditors.find(x => x.document === document)?.revealRange(range, TextEditorRevealType.InCenterIfOutsideViewport);
            // if (diff !== 0) {
            //     commands.executeCommand('cursorMove', {
            //         to: 'down',
            //         by: 'line',
            //         value: diff
            //     });
            // }
            commands.executeCommand('cypresshelper.moveCursorToLine', range.start.line);
            commands.executeCommand('editor.fold', { levels: 1, direction: 'down', selectionLines: positions.map((block) => block.line) });
        }));

    context.subscriptions.push(commands.registerCommand('cypresshelper.moveCursorToLine', 
        (line: number) => {
            const diff = line - (window.activeTextEditor?.selection.active.line ?? line);
            // window.visibleTextEditors.find(x => x.document === document)?.revealRange(range, TextEditorRevealType.InCenterIfOutsideViewport);
            if (diff !== 0) {
                commands.executeCommand('cursorMove', {
                    to: 'down',
                    by: 'line',
                    value: diff
                });
            }
    }));

    createEditCommand(context, 'cypresshelper.addSkip', '.skip');
    createEditCommand(context, 'cypresshelper.removeSkip', '');
    createEditCommand(context, 'cypresshelper.addOnly', '.only');
    createEditCommand(context, 'cypresshelper.removeOnly', '');
}

// this method is called when your extension is deactivated
export function deactivate() { }