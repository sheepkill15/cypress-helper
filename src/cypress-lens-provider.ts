import { CancellationToken, CodeLens, CodeLensProvider,
     Diagnostic, DiagnosticRelatedInformation, DiagnosticSeverity,
      Event, EventEmitter, languages, Location, Position, ProviderResult,
       Range, StatusBarAlignment, TextDocument, window, workspace } from "vscode";

export const getTextWithoutComments = (text: string): string => {
    return text.replace(/('|"|`)[^\1]*?\1/g, (match) => match.replace(/[^\s]/gms, ' '))
            .replace(/\/\/.*/g, (match) => new Array(match.length + 1).join(' '))
            .replace(/\/\*.*?\*\//sg, (match) => match.replace(/[^\s]/gms, ' '));
};

export class CypressLensProvider implements CodeLensProvider {

    constructor() {
        workspace.onDidChangeConfiguration((_) => this._onDidChangeCodeLenses.fire());
    }
    public readonly _onDidChangeCodeLenses = new EventEmitter<void>();
    public readonly onDidChangeCodeLenses?: Event<void> = this._onDidChangeCodeLenses.event;

    private readonly _diagnosticCollection = languages.createDiagnosticCollection('cypresshelper');
    private readonly _statusBarItems = {
        '.skip': window.createStatusBarItem(StatusBarAlignment.Left), 
        '.only': window.createStatusBarItem(StatusBarAlignment.Left) 
    };

    private readonly _fileRegex: string = workspace.getConfiguration().get('conf.cypresshelper.fileFilter') ?? '.*';

    provideCodeLenses(document: TextDocument, _token: CancellationToken): ProviderResult<CodeLens[]> {
        const codeLenses: CodeLens[] = [];
        if(!new RegExp(this._fileRegex).test(document.fileName)) {
            return codeLenses;
        }
        const regex = new RegExp(/\b(describe|context|it) *(\.skip|\.only| *)\(/g);
        const text = getTextWithoutComments(document.getText());
        let matches: RegExpMatchArray | null;

        const foundSkips: Range[] = [];
        const foundOnlys: Range[] = [];
        let prevLine: number = 0;
        let prevCol: number = 0;
        while ((matches = regex.exec(text)) !== null) {
            if(!matches.index) {
                window.showErrorMessage(`CypressHelper: error at file ${document.fileName} after ${prevLine}:${prevCol}`);
                continue;
            }
            const line = document.lineAt(document.positionAt(matches.index).line);
            const indexOf = line.text.indexOf(matches[0]);
            prevLine = line.lineNumber;
            prevCol = indexOf;
            const position = new Position(line.lineNumber, indexOf);
            const range = new Range(position, position.translate(0, matches[0].length - 1));
            const secondRange = new Range(position.translate(0, matches[1].length), range.end);
            if (matches[2] === '.only') {
                codeLenses.push(new CodeLens(range, {
                    title: 'Remove .only',
                    tooltip: `Remove .only call from ${matches[1]}`,
                    command: 'cypresshelper.removeOnly',
                    arguments: [document, secondRange]
                }));
                foundOnlys.push(range);
            } else {
                codeLenses.push(new CodeLens(range, {
                    title: 'Add .only',
                    tooltip: `Add .only call to ${matches[1]}`,
                    command: 'cypresshelper.addOnly',
                    arguments: [document, secondRange]
                }));
            }
            if (matches[2] === '.skip') {
                codeLenses.push(new CodeLens(range, {
                    title: 'Remove .skip',
                    tooltip: `Remove .skip call from ${matches[1]}`,
                    command: 'cypresshelper.removeSkip',
                    arguments: [document, secondRange]
                }));
                foundSkips.push(range);
            } else {
                codeLenses.push(new CodeLens(range, {
                    title: 'Add .skip',
                    tooltip: `Add .skip call to ${matches[1]}`,
                    command: 'cypresshelper.addSkip',
                    arguments: [document, secondRange]
                }));
            }
            if (matches[1] === 'describe' || matches[1] === 'context') {
                codeLenses.push(new CodeLens(range, {
                    title: 'Fold children',
                    tooltip: 'Fold children of this block',
                    command: 'cypresshelper.foldChildren',
                    arguments: [document, range, false]
                }));
                codeLenses.push(new CodeLens(range, {
                    title: 'Fold children recursively',
                    tooltip: 'Fold all children of this block',
                    command: 'cypresshelper.foldChildren',
                    arguments: [document, range, true]
                }));
            }

        }

        const diagnostics: Diagnostic[] = [];

        const addDiagnostic = (array: Range[], name: '.skip' | '.only') => {
            if (array.length > 0) {
                const severity = array.length > 1 ? DiagnosticSeverity.Error : DiagnosticSeverity.Information;
                const message = array.length > 1 ? `There is more than one ${name}` : `There is one ${name}`;

                diagnostics.push({
                    severity,
                    message,
                    range: array[array.length - 1],
                    relatedInformation: array.map(
                        (value, index) => new DiagnosticRelatedInformation(new Location(document.uri, value), `${index + 1}. occurence`))
                });
                this._statusBarItems[name].text = `${array.length} ${name.slice(1)}`;
                this._statusBarItems[name].command = {
                    command: 'cypresshelper.moveCursorToLine',
                    title: 'Move to first',
                    arguments: [array[0].start.line]
                };
                this._statusBarItems[name].show();
            } else {
                this._statusBarItems[name].hide();
            }
        };
        addDiagnostic(foundOnlys, '.only');
        addDiagnostic(foundSkips, '.skip');

        this._diagnosticCollection.set(document.uri, diagnostics);

        return codeLenses;
    }

}