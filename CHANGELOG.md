# Change Log

All notable changes to the "cypresshelper" extension will be documented in this file.

This file adheres to those said in [Keep a Changelog](http://keepachangelog.com/).

## [1.0.4] - 10/07/2023

### Changed

- Regular expressions for the comment and string ignore
- If for some reason a match error occurs, it is now displayed

## [1.0.3] - 11/03/2023

### Changed

- Status bar items are now clickable (move cursor to first instance of .only / .skip, but lose focus)

### Fixed

- RegEx to filter comments

## [1.0.2] - HOTFIX

### Fixed

- Added word boundaries to the regex

## [1.0.1] - HOTFIX

### Changed

- ".only" before ".skip"

## [1.0.0] - 07/08/2022

### Changed

- Complete refactor of all functions
  - All features come from a single provider, updating at the same time
  - New regex matching to ignore comments and for keyword detection
  - Spaces are accounted for
- For now, omitted implementation of 'addBlock' command
- Running the commands from the command runner will throw errors (for now)

### Added

- New snippets for 'it' that contain or not 'AAA'
- Status bar items showing number of skips and only's (if 0 then not shown)

### Fixed

- CodeLens actions sometimes not working in a multi-editor setup
- Unneccessary scrolling when folding

## [0.1.3] - HOTFIX

### Fixed

- Showing folding codelenses on 'it'

## [0.1.2] - 21/11/2021

### Fixed

- Adding .skip | .only removing folding codelenses
- Matching filenames case-sensitive

### Added

- If .skip | .only is already added, adding the other will replace it

## [0.1.1] - 30/10/2021

### Fixed

- Incorrectly matching inside multiline comments

### Added

- Support for browser-based vscode

## [0.1.0] - 29/10/2021

### Fixed

- Incorrectly matching keywords inside comments and strings
- Incorrectly matching variables as keyword invocations
- Multi-editor editing

## [0.0.8] - 22/10/2021

### Fixed

- Recursive and non-recursive search for foldable blocks

## [0.0.7] - 21/10/2021

### Added

- Separate recursive or non-recursive folding
- Snippets for describe|context|it

### Fixed

- Jumping to incorrect cursor location when folding

## [0.0.6] - 20/10/2021

### Fixed

- Falsely showing "Remove .skip|only" when it was in the line elsewhere

## [0.0.5] - 14/10/2021

### Fixed

- Recursively folding the blocks correctly

## [0.0.4] - HOTFIX2

### Changed

- Only folds blocks that are either describe | context | it

## [0.0.3] - HOTFIX

### Fixed

- Folding when there are multiple braces in one line

## [0.0.2] - 05/10/2021

### Added

- Configuration option to filter the files the extension is active in

### Fixed

- Occasionally the parent block is folded together with its children
- Not all blocks were recognized correctly

## [0.0.1] - 02/10/2021

- Initial release
