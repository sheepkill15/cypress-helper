# CypressHelper
### A simple plugin to help your testing process with Cypress

<br>

## Features
- Code lens above code blocks:
    - "Fold children blocks" does what its name suggests, it folds all children blocks of the block you pressed it on. It does *not* fold recursively
    - "Add .only" adds the .only call to the describe | context | it call you pressed it on
    - "Remove .only" appears when there is already a .only call on the block you pressed it on, removes that .only call
    - "Add .skip" adds the .skip call to the describe | context | it call you pressed it on
    - "Remove .skip" appears when there is already a .skip call on the block, removes that .skip call
- Diagnostics about your '.only' | '.skip' calls
    - Shows as information when there is only one of them, nothing when there are none
    - Shows an error message when there are multiple, with reference to each one of them